public class Triangle extends Shape{

    Triangle(double s,double h){
        n = "Triangle";
        side = s;
        height = h;
    }

    void doubleParameters(){
        side *= 2;
        height *= 2;
    }

    double getArea(){
        
        area = 0.5*side*height;
        return area;
        
    }
}
