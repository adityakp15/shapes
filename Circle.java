public class Circle extends Shape{

    Circle (double r){
        n = "Circle";
        radius = r;
    }

    void doubleParameters(){
        radius *= 2;
    }

    double getArea(){
        area = 3.14*radius*radius;
        return area;
    }

}
