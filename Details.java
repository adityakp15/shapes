import java.util.Scanner;

class Coordinates{

    float x,y;

    void setX(float x){
        this.x = x;
    }

    void setY(float y){
        this.y = y;
    }

    float getX(){
        return x;
    }

    float getY(){
        return y;
    }
}

public class Details extends Coordinates{

    String name,color,outline,outlineColor,st;
    float x,y;
    static Scanner s = new Scanner(System.in);
    Object [] ret = new Object[6];
    void setDetails(){
        
        System.out.print("Enter Shape : ");
        name = s.nextLine();
        System.out.print("Enter Color of Shape : ");
        color = s.nextLine();
        System.out.print("Enter thickness of Outline : ");
        outline = s.nextLine();
        System.out.print("Enter Color of Outline : ");
        outlineColor = s.nextLine();
        System.out.print("Enter X Coordinate : ");
        x = s.nextFloat();
        setX(x);
        System.out.print("Enter Y Coordinate : ");
        y = s.nextFloat();
        setY(y);
        st = s.nextLine();
        

    }

    Object[] getDetails(){

        ret[0] = name;
        ret[1] = color;
        ret[2] = outline;
        ret[3] = outlineColor;
        ret[4] = getX();
        ret[5] = getY();

        // for(Object o : ret){
        //     System.out.println(o);
        // }

        // return ret;

        // System.out.println(name);

        // System.out.println(color);

        // System.out.println(ret[4]);

        // System.out.println(ret[5]);

        // System.out.println(outline);

        // System.out.println(outline_color);

        return ret;
    }

}
