public class Square extends Shape{

    Square (double l){
        n = "Square";
        slength = l;
    }

    void doubleParameters(){
        slength *= 2;
    }

    double getArea(){

        area = slength*slength;
        return area;
    }

}
