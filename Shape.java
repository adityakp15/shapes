import java.util.ArrayList;
import java.util.Scanner;

public class Shape {

    String n;
    double area;
    static double radius;
    static double slength;
    static double rlength;
    static double breadth;
    static double side;
    static double height;
    static Scanner s = new Scanner(System.in);
    @Override
    public String toString(){
        return n +" " +area;
    }

    double getArea(){
        return area;
    }

    void doubleParameters(){

    }
    public static void main(String[] args) {

        String name;
        double area;
        String n;
        ArrayList<Shape> shapes= new ArrayList<>();

        String flag = "y";

        while(flag.equalsIgnoreCase("y")){

            System.out.print("Enter the name of the shape : ");
            name = s.nextLine();
            n = name;

            if(name.equalsIgnoreCase("circle")){

                n = name;
                double radius;
                System.out.print("Enter Radius : ");
                radius = s.nextFloat();
                Shape c = new Circle(radius);
                area = c.getArea();
                // System.out.println(c.toString());
                shapes.add(c);

            }
            else if(name.equalsIgnoreCase("square")){

                n = name;
                double length;
                System.out.print("Enter Length : ");
                length = s.nextFloat();
                Shape sq = new Square(length);
                area = sq.getArea();
                // System.out.println(sq.toString());
                // System.out.println(sq.length);
                shapes.add(sq);
            }
            else if(name.equalsIgnoreCase("rectangle")){

                n = name;
                double length,breadth;
                System.out.print("Enter Length : ");
                length = s.nextFloat();
                System.out.print("Enter Breadth : ");
                breadth = s.nextFloat();
                Shape r = new Rectangle(length,breadth);
                area = r.getArea();
                // System.out.println(r.toString());
                shapes.add(r);
            }
            else if(name.equalsIgnoreCase("triangle")){

                n = name;
                double side,height;
                System.out.print("Enter Side : ");
                side = s.nextFloat();
                System.out.print("Enter Height : ");
                height = s.nextFloat();
                Shape t = new Triangle(side,height);
                area = t.getArea();
                // System.out.println(r.toString());
                shapes.add(t);
            }
            
            String st = s.nextLine();
            System.out.print("Continue ? : ");
            flag = s.nextLine();
            System.out.println();

        }

        System.out.println("The Shapes are : ");

        for(int i =0;i<shapes.size();i++){

            System.out.println(shapes.get(i));
            System.out.println();
        }

        ArrayList<Shape> temp= new ArrayList<>();
        for(int i=0;i<shapes.size();i++){
            // if(shapes.get(i) instanceof Circle){
            //     // System.out.println("In Circle");
            //     // System.out.println(radii.get(i).getClass());
            //     // System.out.println("Radius" + shapes.get(i).radius);
            //     Shape c = new Circle(shapes.get(i).radius*2);
            //     c.getArea();
            //     temp.add(c);
            //     // System.out.println(c);
            // }
            // else if(shapes.get(i) instanceof Square){
            //     // System.out.println("In square");
            //     Shape sq = new Square(shapes.get(i).slength*2);
            //     sq.getArea();
            //     // System.out.println(sq.getArea());
            //     temp.add(sq);
            // }
            // else if(shapes.get(i) instanceof Rectangle){
            //     // System.out.println("Rectangle");
            //     Shape r = new Rectangle(shapes.get(i).rlength*2,shapes.get(i).breadth*2);
            //     r.getArea();
            //     // System.out.println(r.getArea());
            //     temp.add(r);
            // }
            // else if(shapes.get(i) instanceof Triangle){
            //     // System.out.println("Rectangle");
            //     Shape t = new Triangle(shapes.get(i).side*2,shapes.get(i).height*2);
            //     t.getArea();
            //     // System.out.println(r.getArea());
            //     temp.add(t);
            // }
            shapes.get(i).doubleParameters();
            shapes.get(i).getArea();
            temp.add(shapes.get(i));

        }

        shapes = temp;

        System.out.println("\nThe Doubled Shapes are : ");

        for(int i =0;i<shapes.size();i++){

            System.out.println(shapes.get(i));
            System.out.println();
        }
        
    }
}
